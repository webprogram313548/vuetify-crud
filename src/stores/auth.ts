import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { User } from '@/types/User'

export const useAuthStore = defineStore('auth', () => {
  const currentUser = ref<User>({
      id: 1,
      email: 'Lion111@gmail.com',
      password: 'Pass@1111',
      fullname: 'Lion King',
      gender: 'male',
      roles: ['user']
  })
  

  return { currentUser }
})
